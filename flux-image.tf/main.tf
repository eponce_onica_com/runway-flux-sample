# Backend setup
terraform {
  backend "s3" {
    key = "flux-image.tfstate"
  }
}

variable "cluster-name" {}

provider "aws" {
  version = "2.53.0-custom"
  region  = "us-east-1"
}

resource "aws_ecr_repository" "flux" {
  name = "flux"
}

data "aws_ecr_authorization_token" "flux" {
  registry_id = aws_ecr_repository.flux.registry_id
}

provider "docker" {
  version = "2.8.0-custom"
  registry_auth {
    address  = data.aws_ecr_authorization_token.flux.proxy_endpoint
    username = data.aws_ecr_authorization_token.flux.user_name
    password = data.aws_ecr_authorization_token.flux.password
  }
}

resource "docker_registry_image" "flux" {
  name = "${aws_ecr_repository.flux.repository_url}:1.15.0"
  keep_remotely = true
  build {
    context = "context"
    suppress_output = true
  }
}

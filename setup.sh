ROOT="$(pwd)"
PLUGIN_FOLDER="$ROOT/flux-image.tf/terraform.d/plugins/linux_amd64"
REPOSITORY_FOLDER="$ROOT/flux-repository"
mkdir -p bin

cd "$ROOT"
AWS_ZIP_PATH="$ROOT/bin/data_source_aws_ecr_authorization_token.zip"
if [ ! -f "$AWS_ZIP_PATH" ]; then
    wget -P ./bin https://github.com/edgarpoce/terraform-provider-aws/archive/feature/data_source_aws_ecr_authorization_token.zip
fi
unzip -q -o $AWS_ZIP_PATH -d "$ROOT/bin/"
cd $ROOT/bin/terraform-provider-aws-feature-data_source_aws_ecr_authorization_token
go build
cp terraform-provider-aws $PLUGIN_FOLDER/terraform-provider-aws_v2.53.0-custom

cd "$ROOT"
DOCKER_ZIP_PATH="$ROOT/bin/runway-flux-example.zip"
if [ ! -f "$DOCKER_ZIP_PATH" ]; then
    wget -P ./bin https://github.com/edgarpoce/terraform-provider-docker/archive/feature/runway-flux-example.zip
fi
unzip -q -o $DOCKER_ZIP_PATH -d "$ROOT/bin/"
cd $ROOT/bin/terraform-provider-docker-feature-runway-flux-example
go build
cp terraform-provider-docker $PLUGIN_FOLDER/terraform-provider-docker_v2.8.0-custom

if [ ! -d "$REPOSITORY_FOLDER" ]; then
cd $ROOT
mkdir -p flux-repository
mkdir -p flux-repository/namespaces
mkdir -p flux-repository/workloads
cat <<EOT > flux-repository/namespaces/demo.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: demo
EOT
cat <<EOT > flux-repository/workloads/readme.md
workloads folder
EOT
cd flux-repository
git init
git add -A
git commit -m "initial commit"
fi

cd $ROOT
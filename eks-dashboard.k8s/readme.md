The dashboard is deployed automatically but manual configuration is required to setup

See https://docs.aws.amazon.com/eks/latest/userguide/dashboard-tutorial.html


# command
to access the dashboard start the kubectl proxy
- kubectl proxy

get the token to login to the dashboard
- kubectl --kubeconfig .kube/dev/config -n kube-system describe secret $(kubectl --kubeconfig .kube/dev/config -n kube-system get secret | grep eks-admin | awk '{print $1}')

The dashboard will be available at
http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/login

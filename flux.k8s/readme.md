# get started

## populate the flux git repository
check you codecommit repository and create two folders in master
- namespaces
- workloads
See the Flux get started repository that can be used as an example
see e.g. demo.yml to deploy a namespace called demo at
https://github.com/fluxcd/flux-get-started/tree/master/namespaces

# sync
Flux syncs automatically every 5 minutes
In order to sync flux ASAP install fluxctl and see the command below to sync

# Useful commands

### check flux status
kubectl -n flux rollout status deployment/flux

### get flux public key
fluxctl identity --k8s-fwd-labels name=flux --k8s-fwd-ns flux

### check flux deployment status
runway kbenv run -- -n flux rollout status deployment flux

## restart deployment after changing configmap
runway kbenv run -- -n flux rollout restart deployment flux

## manually sync flux
fluxctl sync --k8s-fwd-labels name=flux --k8s-fwd-ns flux

### logs 
kubectl logs -f -lname=flux -n flux

### exec
kubectl exec -it flux-66f6c5c994-zfc55 -n flux -- /bin/bash
terraform {
  backend "s3" {
    key = "flux-base.tfstate"
  }
}

variable "KUBECONFIG" {}
variable "region" {}
variable "cluster-name" {}

provider "kubernetes" {
  load_config_file = true
  config_path      = var.KUBECONFIG
  version          = "1.10.0"
}

provider "aws" {
  version = "~> 2.28"
  region  = var.region
}

data "aws_ssm_parameter" "oidc_iam_provider_cluster_url" {
  name  = "/${var.cluster-name}/oidc-iam-provider-cluster-url"
}

data "aws_ssm_parameter" "oidc_iam_provider_cluster_arn" {
  name  = "/${var.cluster-name}/oidc-iam-provider-cluster-arn"
}

resource "random_string" "flux_random" {
  length  = 16
  special = false
}

resource "kubernetes_namespace" "flux" {
  metadata {
    name = "flux"
  }
}
resource "aws_codecommit_repository" "flux_repository" {
  repository_name = "${var.cluster-name}-flux-${random_string.flux_random.result}"
}

data "aws_iam_policy_document" "flux_policy" {

  statement {
    effect = "Allow"
    actions = [
      "codecommit:*"
    ]
    resources = [
      "${aws_codecommit_repository.flux_repository.arn}"
    ]
  }

}

data "aws_iam_policy_document" "flux_service_account_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    condition {
      test     = "StringEquals"
      variable = "${replace(data.aws_ssm_parameter.oidc_iam_provider_cluster_url.value, "https://", "")}:sub"
      values   = ["system:serviceaccount:flux:flux"]
    }

    principals {
      identifiers = ["${data.aws_ssm_parameter.oidc_iam_provider_cluster_arn.value}"]
      type        = "Federated"
    }
  }
}

resource "aws_iam_role" "flux_service_account" {
  name               = "flux-${terraform.workspace}"
  assume_role_policy = data.aws_iam_policy_document.flux_service_account_assume_role_policy.json
}

resource "aws_iam_role_policy" "flux_service_account" {
  role   = aws_iam_role.flux_service_account.id
  policy = data.aws_iam_policy_document.flux_policy.json
}

resource "kubernetes_service_account" "flux_service_account" {
  automount_service_account_token = true
  metadata {
    name      = "flux"
    namespace = "flux"
    labels = {
      app  = "flux"
      name = "flux"
    }
    annotations = {
      "eks.amazonaws.com/role-arn" = aws_iam_role.flux_service_account.arn
    }
  }
  depends_on = [
    aws_iam_role_policy.flux_service_account,
  ]
}

resource "kubernetes_config_map" "flux_git_config" {
  metadata {
    name      = "flux-git-config"
    namespace = "flux"
    labels = {
      app  = "flux"
      name = "flux-git-config"
    }
  }
  data = {
    giturl    = aws_codecommit_repository.flux_repository.clone_url_http
    gitconfig = <<EOF
     [credential "${aws_codecommit_repository.flux_repository.clone_url_http}"]
       helper = !AWS_WEB_IDENTITY_TOKEN_FILE=/var/run/secrets/eks.amazonaws.com/serviceaccount/token AWS_ROLE_ARN=${aws_iam_role.flux_service_account.arn} aws codecommit credential-helper $@
       UseHttpPath = true
    EOF
  }
}

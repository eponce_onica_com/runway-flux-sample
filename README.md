# Instructions to deploy
- run ./setup.sh
- update the kube access role in runway.yml
- update AWS account in image property at flux.k8s/base/flux.yaml
- run DEPLOY_ENVIRONMENT=dev runway deploy

# initial repository setup after deployment
- cd flux-repository
- git remote add origin {repository_clone_url}
- git push --set-upstream origin master

# modules
- eks-base: eks cluster
- eks-dashboard.k8s: eks dashboard (too insecure, too broad permissions)
- flux-image.tf: module that creates the image (custom terraform features, PRs in review)
- flux-base.tf: resources necessary to run flux
- flux.k8s: flux app

